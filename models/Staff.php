<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "staff".
 *
 * @property integer $id
 * @property integer $parentid
 * @property integer $parentname
 * @property string $firstname
 * @property string $lastname
 * @property string $position
 * @property string $email
 * @property string $homephone
 * @property string $notes
 */
class Staff extends ActiveRecord
{


	          /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * @inheritdoc    */
    public function rules()
    {
        return [
            [['parentid'], 'integer'],
            [['firstname', 'lastname', 'position', 'email', 'homephone'], 'required'],
            [['notes', 'parentname'], 'string'],
	        [['email'],'email'],
            [['firstname', 'lastname', 'position', 'email', 'homephone'], 'string', 'max' => 50],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parentid' => 'Его Руководитель',
	        'parentname' =>'Его Руководитель',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'position' => 'Должность',
            'email' => 'Email',
            'homephone' => 'Телефон',
            'notes' => 'Примечания',
        ];
    }

	public function getDDListInfo(){

		// поставить проверки на результаты запросов

		$listFirstName = Staff::find()->select(['firstname', 'id'])->indexBy('id')->column() ;
		$listLastName = Staff::find()->select(['lastname', 'id'])->indexBy('id')->column() ;
		$listposition = Staff::find()->select(['position', 'id'])->indexBy('id')->column() ;

		foreach ($listLastName as $i => $value) {
			$finallist[$i] = $listLastName[$i].' '.$listFirstName[$i].'  ::  '.$listposition[$i];
		}


		return $finallist	;
	}
	public function getDDList($id){

		// поставить проверки на результаты запросов

		$listFirstName = Staff::find()->select(['firstname', 'id'])->where(['id'=> !$id])->indexBy('id')->column() ;
		$listLastName = Staff::find()->select(['lastname', 'id'])->where(['id'=> !$id])->indexBy('id')->column() ;
		$listposition = Staff::find()->select(['position', 'id'])->where(['id'=> !$id])->indexBy('id')->column() ;

		foreach ($listLastName as $i => $value) {
			$finallist[$i] = $listLastName[$i].' '.$listFirstName[$i].'  ::  '.$listposition[$i];
		}


		return $finallist	;
	}

	public function getParentName ($id){

		$pnQuery = Staff::find()->where(['id' => $id])->one() ;

		return $pnQuery['lastname'].' '.$pnQuery['firstname'].'  ::  '.$pnQuery['position'] ;


	}

}
