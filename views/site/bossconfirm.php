
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\ActiveRecord ;
use app\models\Staff ;

?>

<div class="jumbotron">
	<h2>Отдел кадров компании  'Забота'</h2>
	<p class="lead">Вы успешно ввели самого главного начальника. !</p>
	<h5>(При необходимости его данные можно корректировать из общей структуры компании)</h5>
</div>

<?php

	$boss = 'Это - самый главный начальник' ;
	$model->parentid = $boss ;

?>
<?=	DetailView::widget([
	'model' => $model,
	'attributes' => [
		//'id',
		'lastname',
		'firstname',
		'parentid',
		'position',
		'email:email',
		'homephone',
		'notes:ntext',
	],
]) ?>

<p style="text-align: center">
	<a class="btn btn-lg btn-success" href="index.php">Показать структуру</a>
</p>