<?php
/*View для ввода данных о новом сотруднике*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Staff;

?>
<div class="jumbotron">
	<h2>Отдел кадров компании  'Забота'</h2>
	<p class="lead">Форма для ввода данных нового сотрудника</p>
	<h5>(При необходимости его данные можно корректировать позже из общей структуры компании)</h5>
</div>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'firstname')->label('Имя сотрудника'); ?>
	<?= $form->field($model, 'lastname')->label('Фамилия сотрудника'); ?>
	<?= $form->field($model, 'position')->label('Должность'); ?>

	<?= $ddlQuery = Staff::getDDListInfo() ;?>

	<?= $form->field($model, 'parentid')->dropdownList($ddlQuery,['prompt'=>'Список руководителей'])->label('Фамилия непосредственного руководителя');?>

	<?= $form->field($model, 'email')->label('E-mail'); ?>
	<?= $form->field($model, 'homephone')->label('Номер домашнего телефона'); ?>
	<?= $form->field($model, 'notes')->label('Заметки о нем'); ?>

<div class="form-group">
	<?= Html::submitButton('Записать', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
