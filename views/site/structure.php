<?php


use \app\models\Staff;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$currentid = -1 ;
$text = 0  ;

$mysqli = new mysqli('localhost', 'root', '', 'basictree');
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}


?>

<div class="jumbotron">
    <h2>Отдел кадров компании  'Забота'</h2>
    <p class="lead">Общая структура компании</p>
</div>

<?php

buildTree($mysqli,$currentid) ;

function buildTree($mysqli,$parentid)
{

    global $currentid;
    global $text;


    $queryid = 'SELECT * FROM staff WHERE parentid = ' . $parentid;

    if (($result = $mysqli->query($queryid))) { // Если запрос прошел успешно

        while ($person = $result->fetch_assoc()) {


            if ($person['parentid'] == -1) { // Если это Биг Босс

                echo '<p><strong><font size="5">' . $person['lastname'] . '  ' . $person['firstname'] .
                     ' ::  ' . $person['position'] . '</font></strong> </p>';

            } elseif ($person['parentid'] == $currentid) {  //Если уровень текущий - оставляем без сдвига

                $textindent = " " . $text;
                echo '<p style="text-indent:' . $textindent . 'px;">' . '<font style="color:#3EB0D6;"> I_____ </font><em><strong><font size="3">' . $person['lastname'] .
                     '  ' . $person['firstname'] . '</strong>  ::  ' . $person['position'] . '</em></font></p>';

            } elseif ($person['parentid'] > $currentid) {  // Уровень ниже - сдвигаем вправо

                $text = $text + 40;
                $textindent = " " . $text;
                echo '<p style="text-indent:' . $textindent . 'px;">' . '<font style="color:#3EB0D6;">I_____ </font><em><strong><font size="3">' . $person['lastname'] .
                     '  ' . $person['firstname'] . '</strong>  ::  ' . $person['position'] . '</em></font></p>';

            } elseif ($person['parentid'] < $currentid) {  // Уровень выше - сдвигаем вправо

                $text = $text - 40;
                $textindent = " " . $text;
                echo '<p style="text-indent:' . $textindent . 'px;">' . '<font style="color:#3EB0D6;">I_____ </font><em><strong><font size="3">' . $person['lastname'] .
                     '  ' . $person['firstname'] . '</strong>   ::  ' . $person['position'] . '</em></font></p>';
            }

            $currentid = $person['parentid'];

            buildTree($mysqli, $person['id']);

        }


    }
}
?>