<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \app\models\Staff;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

?>

<div class="jumbotron">
    <h2>Отдел кадров компании  'Забота'</h2>
    <p class="lead">Личная карта сотрудника<?php echo " ".$model->lastname." ".$model->firstname ;?></p>
</div>


<div class="staff-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'lastname',
            'firstname',
            'position',
            //'parentid',
            'parentname',
            'email:email',
            'homephone',
            'notes:ntext',
        ],
    ]) ?>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены в удалении сотрудника'.'  '.$model->lastname.' '.$model->firstname." ?",
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
