<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Staff ;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="jumbotron">
        <h2>Отдел кадров компании  'Забота'</h2>
        <p class="lead">Форма для редактирования данных сотрудника <?php echo " ".$model->lastname." ".$model->firstname ;?></p>
    </div>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'firstname')->label('Имя сотрудника'); ?>
<?= $form->field($model, 'lastname')->label('Фамилия сотрудника'); ?>
<?= $form->field($model, 'position')->label('Должность'); ?>

<?= $ddlQuery = Staff::getDDListInfo() ;?>

<?php if($model['parentid'] > 0) : ?>   <!--Если эт не Биг Босс, то показываем список начальников   -->

    <?= $form->field($model, 'parentid')->dropdownList($ddlQuery,['prompt'=>'Список руководителей'])->label('Фамилия непосредственного руководителя');?>

<?php endif ;?>

<?= $form->field($model, 'email')->label('E-mail'); ?>
<?= $form->field($model, 'homephone')->label('Номер домашнего телефона'); ?>
<?= $form->field($model, 'notes')->label('Заметки о нем'); ?>

    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>