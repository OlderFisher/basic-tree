<?php

use yii\helpers\Html;
use \app\models\Staff;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

?>
<div class="staff-update">

    <?= $this->render('_form', [ 'model' => $model,]) ?>
</div>
